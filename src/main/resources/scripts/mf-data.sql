-- MySQL dump 10.13  Distrib 8.0.23, for Linux (x86_64)
--
-- Host: localhost    Database: milk_factory
-- ------------------------------------------------------
-- Server version	5.6.48

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` VALUES (1,'Company 1','1234','1234','1234','company1@gmail.com','www.company1.com'),(2,'Company2','1235','1235','1235','comapny2@gmail.com','www.company2.com');
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `customer_receipt`
--

LOCK TABLES `customer_receipt` WRITE;
/*!40000 ALTER TABLE `customer_receipt` DISABLE KEYS */;
INSERT INTO `customer_receipt` VALUES (1,'2020-11-10 23:00:00','note 1',1,1,1,'Mika Mikic'),(2,'2020-12-09 23:00:00','note 2',1,1,1,'Pera Peric'),(3,'2020-01-09 23:00:00','note 2',2,2,1,'Mika Mikic'),(6,'2020-01-09 23:00:00','note 4',2,2,2,'Pera Peric');
/*!40000 ALTER TABLE `customer_receipt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `customer_receipt_item`
--

LOCK TABLES `customer_receipt_item` WRITE;
/*!40000 ALTER TABLE `customer_receipt_item` DISABLE KEYS */;
INSERT INTO `customer_receipt_item` VALUES (1,1,100.00,5.00,95.00,2),(2,2,150.00,5.00,95.00,1),(3,3,150.00,5.00,95.00,1),(4,3,50.00,5.00,195.00,2),(23,6,150.00,5.00,95.00,1),(24,6,50.00,5.00,195.00,2);
/*!40000 ALTER TABLE `customer_receipt_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `customer_receipt_payment_method_aggr`
--

LOCK TABLES `customer_receipt_payment_method_aggr` WRITE;
/*!40000 ALTER TABLE `customer_receipt_payment_method_aggr` DISABLE KEYS */;
INSERT INTO `customer_receipt_payment_method_aggr` VALUES (1,1,2),(2,2,2),(3,1,3),(14,1,6);
/*!40000 ALTER TABLE `customer_receipt_payment_method_aggr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `delivery_note`
--

LOCK TABLES `delivery_note` WRITE;
/*!40000 ALTER TABLE `delivery_note` DISABLE KEYS */;
INSERT INTO `delivery_note` VALUES (1,'Pera Peric','2020-02-02 00:00:00',1,2,1),(2,'Mika Mikic','2021-10-01 00:00:00',2,1,2);
/*!40000 ALTER TABLE `delivery_note` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `delivery_note_item`
--

LOCK TABLES `delivery_note_item` WRITE;
/*!40000 ALTER TABLE `delivery_note_item` DISABLE KEYS */;
INSERT INTO `delivery_note_item` VALUES (1,1,20.00,1),(2,2,30.00,2),(3,1,22.00,2);
/*!40000 ALTER TABLE `delivery_note_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `measurement_unit`
--

LOCK TABLES `measurement_unit` WRITE;
/*!40000 ALTER TABLE `measurement_unit` DISABLE KEYS */;
INSERT INTO `measurement_unit` VALUES (1,'KG'),(2,'L'),(3,'M');
/*!40000 ALTER TABLE `measurement_unit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `payment_method`
--

LOCK TABLES `payment_method` WRITE;
/*!40000 ALTER TABLE `payment_method` DISABLE KEYS */;
INSERT INTO `payment_method` VALUES (1,'pun iznos'),(2,'popust'),(3,'odlozeno');
/*!40000 ALTER TABLE `payment_method` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,'Product 1','2021-04-03 21:10:58',100.00,'1',2000.00,2,1),(2,'Product 2','2021-04-03 21:11:22',200.00,'2',3000.00,2,2),(3,'Product 3','2021-04-03 21:20:53',300.00,'1',1000.00,1,1);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `product_type`
--

LOCK TABLES `product_type` WRITE;
/*!40000 ALTER TABLE `product_type` DISABLE KEYS */;
INSERT INTO `product_type` VALUES (1,'Milk'),(2,'Yoghurt');
/*!40000 ALTER TABLE `product_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `quality_certificate`
--

LOCK TABLES `quality_certificate` WRITE;
/*!40000 ALTER TABLE `quality_certificate` DISABLE KEYS */;
INSERT INTO `quality_certificate` VALUES (1,'description 1','2021-12-02 23:00:00',2,2,2),(2,'description 2','2020-12-02 23:00:00',1,1,1),(5,'description 3','2020-12-02 23:00:00',1,1,1),(6,'description 4','2020-12-02 23:00:00',1,1,1),(7,'description 5','2020-12-02 23:00:00',3,1,1);
/*!40000 ALTER TABLE `quality_certificate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `quality_check_request`
--

LOCK TABLES `quality_check_request` WRITE;
/*!40000 ALTER TABLE `quality_check_request` DISABLE KEYS */;
INSERT INTO `quality_check_request` VALUES (1,'2021-02-02 00:00:00','note 1',1,1,1),(2,'2020-01-01 00:00:00','note 2',2,2,2),(3,'2021-04-04 00:00:00','note 3',1,1,1);
/*!40000 ALTER TABLE `quality_check_request` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `unit`
--

LOCK TABLES `unit` WRITE;
/*!40000 ALTER TABLE `unit` DISABLE KEYS */;
INSERT INTO `unit` VALUES (1,'Unit 1'),(2,'Unit 2');
/*!40000 ALTER TABLE `unit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `veterinarian`
--

LOCK TABLES `veterinarian` WRITE;
/*!40000 ALTER TABLE `veterinarian` DISABLE KEYS */;
INSERT INTO `veterinarian` VALUES (1,'Vet 1','1','1','123','company1@gmail.com','www.vet1.com'),(2,'Vet 2','2','2','124','company2@gmail.com','www.vet2.com'),(3,'Vet 3','3','3','125','company3@gmail.com','www.vet3.com');
/*!40000 ALTER TABLE `veterinarian` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `worker`
--

LOCK TABLES `worker` WRITE;
/*!40000 ALTER TABLE `worker` DISABLE KEYS */;
INSERT INTO `worker` VALUES (1,'1234','1234','John Johnosn',1),(2,'1235','1235','Peter Peterson',2),(3,'1236','1236','Harry Harrison',1);
/*!40000 ALTER TABLE `worker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `workplace`
--

LOCK TABLES `workplace` WRITE;
/*!40000 ALTER TABLE `workplace` DISABLE KEYS */;
INSERT INTO `workplace` VALUES (1,'Workplace 1','workplace 1',1),(2,'Workplace 2','workplace 2',2);
/*!40000 ALTER TABLE `workplace` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-19 19:10:50
