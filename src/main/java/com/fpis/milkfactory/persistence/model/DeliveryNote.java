package com.fpis.milkfactory.persistence.model;


import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "delivery_note")
public class DeliveryNote extends BaseDomainModel{

    @Column(name = "received_by")
    private String receivedBy;

    @Column(name = "dispatch_date")
    private Date dispatchDate;

    @ManyToOne
    @JoinColumn(name = "worker_delivery_id", nullable = false)
    private Worker workerDelivered;

    @ManyToOne
    @JoinColumn(name = "worker_issued_id", nullable = false)
    private Worker workerIssued;

    @ManyToOne
    @JoinColumn(name = "customer_id", nullable = false)
    private Customer customer;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "deliveryNote")
    private List<DeliveryNoteItem> items = new ArrayList<>();

    public String getReceivedBy() {
        return receivedBy;
    }

    public void setReceivedBy(String receivedBy) {
        this.receivedBy = receivedBy;
    }

    public Date getDispatchDate() {
        return dispatchDate;
    }

    public void setDispatchDate(Date dispatchDate) {
        this.dispatchDate = dispatchDate;
    }

    public List<DeliveryNoteItem> getItems() {
        return items;
    }

    public void setItems(List<DeliveryNoteItem> items) {
        this.items = items;
    }

    public Worker getWorkerDelivered() {
        return workerDelivered;
    }

    public void setWorkerDelivered(Worker workerDelivered) {
        this.workerDelivered = workerDelivered;
    }

    public Worker getWorkerIssued() {
        return workerIssued;
    }

    public void setWorkerIssued(Worker workerIssued) {
        this.workerIssued = workerIssued;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}
