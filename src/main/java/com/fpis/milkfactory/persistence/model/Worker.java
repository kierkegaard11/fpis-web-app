package com.fpis.milkfactory.persistence.model;

import javax.persistence.*;

@Entity
@Table(name = "worker")
public class Worker extends BaseDomainModel{

    @Column(name = "employment_number")
    private String employmentNumber;

    @Column(name = "umcn")
    private String umcn;

    @Column(name = "name_lastname")
    private String firstLastName;

    @ManyToOne
    @JoinColumn(name = "workplace_id", nullable = false)
    private Workplace workplace;

    public String getEmploymentNumber() {
        return employmentNumber;
    }

    public void setEmploymentNumber(String employmentNumber) {
        this.employmentNumber = employmentNumber;
    }

    public String getUmcn() {
        return umcn;
    }

    public void setUmcn(String umcn) {
        this.umcn = umcn;
    }

    public String getFirstLastName() {
        return firstLastName;
    }

    public void setFirstLastName(String firstLastName) {
        this.firstLastName = firstLastName;
    }

    public Workplace getWorkplace() {
        return workplace;
    }

    public void setWorkplace(Workplace workplace) {
        this.workplace = workplace;
    }
}
