package com.fpis.milkfactory.persistence.model;


import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Table(name = "quality_certificate")
public class QualityCertificate extends BaseDomainModel{

    @Column(name = "description")
    private String description;

    @Column(name = "date")
    private Date date;

    @ManyToOne
    @JoinColumn(name = "quality_check_request_id", nullable = false)
    private QualityRequest qualityRequest;

    @ManyToOne
    @JoinColumn(name = "veterinarian_id", nullable = false)
    private Veterinarian veterinarian;

    @ManyToOne
    @JoinColumn(name = "product_id", nullable = false)
    private Product product;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public QualityRequest getQualityRequest() {
        return qualityRequest;
    }

    public void setQualityRequest(QualityRequest qualityRequest) {
        this.qualityRequest = qualityRequest;
    }

    public QualityRequest getQualityCheckRequest() {
        return qualityRequest;
    }

    public void setQualityCheckRequest(QualityRequest qualityRequest) {
        this.qualityRequest = qualityRequest;
    }

    public Veterinarian getVeterinarian() {
        return veterinarian;
    }

    public void setVeterinarian(Veterinarian veterinarian) {
        this.veterinarian = veterinarian;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
