package com.fpis.milkfactory.persistence.model;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "customer_receipt")
public class CustomerReceipt extends BaseDomainModel{

    @Column(name = "due_date")
    private Date dueDate;

    @Column(name = "note")
    private String note;

    @Column(name = "received_by")
    private String personReceived;

    @ManyToOne
    @JoinColumn(name = "customer_id", nullable = false)
    private Customer customer;

    @ManyToOne
    @JoinColumn(name = "worker_id", nullable = false)
    private Worker worker;

    @ManyToOne
    @JoinColumn(name = "delivery_note_id", nullable = false)
    private DeliveryNote deliveryNote;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "customerReceipt")
    private List<CustomerReceiptItem> items = new ArrayList<>();

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Worker getWorker() {
        return worker;
    }

    public void setWorker(Worker worker) {
        this.worker = worker;
    }

    public DeliveryNote getDeliveryNote() {
        return deliveryNote;
    }

    public void setDeliveryNote(DeliveryNote deliveryNote) {
        this.deliveryNote = deliveryNote;
    }

    public List<CustomerReceiptItem> getItems() {
        return items;
    }

    public void setItems(List<CustomerReceiptItem> items) {
        this.items = items;
    }

    public String getPersonReceived() {
        return personReceived;
    }

    public void setPersonReceived(String personReceived) {
        this.personReceived = personReceived;
    }
}
