package com.fpis.milkfactory.persistence.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "customer_receipt_payment_method_aggr")
public class CustomerReceiptPaymentMethodAggr extends BaseDomainModel{

    @ManyToOne
    @JoinColumn(name = "customer_receipt_id", nullable = false)
    private CustomerReceipt customerReceipt;

    @ManyToOne
    @JoinColumn(name = "payment_method_id", nullable = false)
    private PaymentMethod paymentMethod;

    public CustomerReceipt getCustomerReceipt() {
        return customerReceipt;
    }

    public void setCustomerReceipt(CustomerReceipt customerReceipt) {
        this.customerReceipt = customerReceipt;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
}
