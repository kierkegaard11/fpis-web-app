package com.fpis.milkfactory.persistence.model;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity()
@Table(name = "customer_receipt_item")
public class CustomerReceiptItem extends BaseDomainModel{

    @ManyToOne
    @JoinColumn(name = "customer_receipt_id", nullable = false)
    private CustomerReceipt customerReceipt;

    @Column(name = "quantity")
    private BigDecimal quantity;

    @Column(name = "rebate")
    private BigDecimal rebate;

    @Column(name = "price_with_discount")
    private BigDecimal priceWithDiscount;

    @ManyToOne
    @JoinColumn(name = "product_id", nullable = false)
    private Product product;

    public CustomerReceipt getCustomerReceipt() {
        return customerReceipt;
    }

    public void setCustomerReceipt(CustomerReceipt customerReceipt) {
        this.customerReceipt = customerReceipt;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getRebate() {
        return rebate;
    }

    public void setRebate(BigDecimal rebate) {
        this.rebate = rebate;
    }

    public BigDecimal getPriceWithDiscount() {
        return priceWithDiscount;
    }

    public void setPriceWithDiscount(BigDecimal priceWithDiscount) {
        this.priceWithDiscount = priceWithDiscount;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

}
