package com.fpis.milkfactory.persistence.dao;


import com.fpis.milkfactory.persistence.model.DeliveryNote;
import com.fpis.milkfactory.persistence.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductDAO extends JpaRepository<Product, Long>, JpaSpecificationExecutor<Product> {

    @Query(value = "SELECT p FROM Product p " +
            "WHERE (:name IS NULL OR p.name= :name)" +
            "AND (:productId IS NULL OR p.id = :productId)")
    List<Product> findAllByNameAndId(@Param("productId") Long productId, @Param("name") String name);

}
