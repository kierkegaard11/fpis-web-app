package com.fpis.milkfactory.persistence.dao;

import com.fpis.milkfactory.persistence.model.CustomerReceipt;
import com.fpis.milkfactory.persistence.model.CustomerReceiptItem;
import com.fpis.milkfactory.persistence.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Repository
public interface CustomerReceiptItemDAO extends JpaRepository<CustomerReceiptItem, Long>, JpaSpecificationExecutor<CustomerReceiptItem> {

    @Query("SELECT c FROM CustomerReceiptItem c WHERE c.customerReceipt.id= ?1")
    List<CustomerReceiptItem> getByCustomerReceiptId(long customerReceiptId);

    Optional<CustomerReceiptItem> getById(Long id);

    @Query("UPDATE CustomerReceiptItem c SET c.product=?2, c.quantity=?3, c.rebate=?4, c.priceWithDiscount=?5 WHERE c.id=?1")
    void update(Long id, Product product, BigDecimal quantity, BigDecimal rebate, BigDecimal priceWithDiscount);
}
