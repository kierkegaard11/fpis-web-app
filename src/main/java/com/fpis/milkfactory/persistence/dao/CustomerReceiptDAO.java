package com.fpis.milkfactory.persistence.dao;

import com.fpis.milkfactory.dto.CustomerReceiptDTO;
import com.fpis.milkfactory.persistence.model.CustomerReceipt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerReceiptDAO extends JpaRepository<CustomerReceipt, Long>, JpaSpecificationExecutor<CustomerReceipt> {

    @Query(value = "SELECT c FROM CustomerReceipt c " +
            "WHERE (:year IS NULL OR EXTRACT (year FROM c.dueDate) = :year)" +
            " AND (:month IS NULL OR EXTRACT (month FROM c.dueDate) = :month)" +
            " AND (:day IS NULL OR EXTRACT (day FROM c.dueDate) = :day)" +
            "AND (:customerReceiptId IS NULL OR c.id = :customerReceiptId)")
    List<CustomerReceipt> findAllByIdAndDate(@Param("customerReceiptId") Long customerReceiptId,
                                                @Param("day") Integer day,
                                                @Param("month") Integer month,
                                                @Param("year") Integer year);
}
