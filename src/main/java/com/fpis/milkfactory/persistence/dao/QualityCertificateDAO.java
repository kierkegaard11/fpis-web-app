package com.fpis.milkfactory.persistence.dao;

import com.fpis.milkfactory.persistence.model.QualityCertificate;
import com.fpis.milkfactory.persistence.model.QualityRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface QualityCertificateDAO extends JpaRepository<QualityCertificate, Long>, JpaSpecificationExecutor<QualityCertificate> {

    @Query(value = "SELECT q FROM QualityCertificate q " +
            "WHERE (:year IS NULL OR EXTRACT (year FROM q.date) = :year)" +
            " AND (:month IS NULL OR EXTRACT (month FROM q.date) = :month)" +
            " AND (:day IS NULL OR EXTRACT (day FROM q.date) = :day)" +
            "AND (:certificateId IS NULL OR q.id = :certificateId)")
    List<QualityCertificate> findAllByIdAndDate(@Param("certificateId") Long certificateId,
                                                @Param("day") Integer day,
                                                @Param("month") Integer month,
                                                @Param("year") Integer year);

    @Query("SELECT q FROM QualityCertificate q WHERE q.id= ?1")
    Optional<Object> findByIdAndShit(Long certificateId);
}
