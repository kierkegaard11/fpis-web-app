package com.fpis.milkfactory.persistence.dao;

import com.fpis.milkfactory.persistence.model.DeliveryNote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DeliveryNoteDAO  extends JpaRepository<DeliveryNote, Long>, JpaSpecificationExecutor<DeliveryNote> {

    @Query(value = "SELECT d FROM DeliveryNote d " +
            "WHERE (:year IS NULL OR EXTRACT (year FROM d.dispatchDate) = :year)" +
            " AND (:month IS NULL OR EXTRACT (month FROM d.dispatchDate) = :month)" +
            " AND (:day IS NULL OR EXTRACT (day FROM d.dispatchDate) = :day)" +
            "AND (:noteId IS NULL OR d.id = :noteId)")
    List<DeliveryNote> findAllByIdAndDate(@Param("noteId") Long noteId,
                                          @Param("day") Integer day,
                                          @Param("month") Integer month,
                                          @Param("year") Integer year);
}
