package com.fpis.milkfactory.persistence.dao;

import com.fpis.milkfactory.persistence.model.QualityRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QualityRequestDAO extends JpaRepository<QualityRequest, Long>, JpaSpecificationExecutor<QualityRequest> {

    @Query(value = "SELECT q FROM QualityRequest q " +
            "WHERE (:year IS NULL OR EXTRACT (year FROM q.date) = :year)" +
            " AND (:month IS NULL OR EXTRACT (month FROM q.date) = :month)" +
            " AND (:day IS NULL OR EXTRACT (day FROM q.date) = :day)" +
            "AND (:requestId IS NULL OR q.id = :requestId)")
    List<QualityRequest> findAllByIdAndDate(@Param("requestId") Long requestId,
                                            @Param("day") Integer day,
                                            @Param("month") Integer month,
                                            @Param("year") Integer year);
}
