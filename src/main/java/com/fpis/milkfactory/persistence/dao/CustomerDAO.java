package com.fpis.milkfactory.persistence.dao;

import com.fpis.milkfactory.persistence.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerDAO extends JpaRepository<Customer, Long>, JpaSpecificationExecutor<Customer> {
}
