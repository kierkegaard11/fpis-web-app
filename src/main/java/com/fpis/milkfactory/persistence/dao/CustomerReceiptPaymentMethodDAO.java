package com.fpis.milkfactory.persistence.dao;

import com.fpis.milkfactory.persistence.model.CustomerReceiptPaymentMethodAggr;
import com.fpis.milkfactory.persistence.model.DeliveryNote;
import com.fpis.milkfactory.persistence.model.PaymentMethod;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerReceiptPaymentMethodDAO extends JpaRepository<CustomerReceiptPaymentMethodAggr, Long>, JpaSpecificationExecutor<CustomerReceiptPaymentMethodAggr> {
    @Query("SELECT c FROM CustomerReceiptPaymentMethodAggr c WHERE c.customerReceipt.id= ?1")
    List<CustomerReceiptPaymentMethodAggr> findByCustomerReceiptId(Long customerReceiptId);
}
