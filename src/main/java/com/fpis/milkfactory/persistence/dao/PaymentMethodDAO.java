package com.fpis.milkfactory.persistence.dao;

import com.fpis.milkfactory.persistence.model.DeliveryNote;
import com.fpis.milkfactory.persistence.model.PaymentMethod;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentMethodDAO extends JpaRepository<PaymentMethod, Long>, JpaSpecificationExecutor<PaymentMethod> {

}
