package com.fpis.milkfactory.persistence.dao;

import com.fpis.milkfactory.persistence.model.Worker;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface WorkerDAO extends JpaRepository<Worker, Long>, JpaSpecificationExecutor<Worker> {
}
