package com.fpis.milkfactory.util;

public enum MFErrors {

    INVALID_ENTITY_ID(10011, "INFO"),
    INPUT_VALIDATION_ERROR(10081, "INFO");

    private int code;
    private String level;

    MFErrors(int code, String level) {
        this.code = code;
        this.level = level;
    }

    public int getCode(){
        return this.code;
    }

    public String getLevel(){
        return this.level;
    }

    public boolean isError() {
        return this.level.equalsIgnoreCase("ERROR");
    }
}
