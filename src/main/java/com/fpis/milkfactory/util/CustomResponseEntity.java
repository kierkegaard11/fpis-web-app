package com.fpis.milkfactory.util;

import com.fpis.milkfactory.exception.MFBusinessException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;

public final class CustomResponseEntity {

    private CustomResponseEntity() {
    }

    public static ResponseEntity<?> success(Object data) {
        return ResponseEntity.ok(new CustomResponse(data, 10000));
    }

    public static ResponseEntity<?> success(Page page) {
        Map<String, Object> response = new HashMap<>();
        response.put("items", page.getContent());
        response.put("currentPage", page.getNumber());
        response.put("totalItems", page.getTotalElements());
        response.put("totalPages", page.getTotalPages());
        return ResponseEntity.ok(new CustomResponse(response, 10000));
    }

    public static ResponseEntity<?> error(Map<String, String> map) {
        return new ResponseEntity<>(new CustomResponse(map, MFErrors.INPUT_VALIDATION_ERROR.getCode()), HttpStatus.CONFLICT);
    }

    public static ResponseEntity<?> error(MFBusinessException dcbe) {
        return new ResponseEntity<>(new CustomResponse(dcbe.getMessage(), dcbe.getStatus(), Util.getStackTraceString(dcbe)), HttpStatus.BAD_REQUEST);
    }

    public static ResponseEntity<?> error(RuntimeException dcbe) {
        return new ResponseEntity<>(new CustomResponse(dcbe.getMessage(), 10001, Util.getStackTraceString(dcbe)), HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
