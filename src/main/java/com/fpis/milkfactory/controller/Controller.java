package com.fpis.milkfactory.controller;

import com.fpis.milkfactory.controller.validator.ApiObjectValidator;
import com.fpis.milkfactory.dto.*;
import com.fpis.milkfactory.exception.MFBusinessException;
import com.fpis.milkfactory.facade.PaymentMethodFacade;
import com.fpis.milkfactory.facade.QualityCertificateFacade;
import com.fpis.milkfactory.facade.QualityRequestFacade;
import com.fpis.milkfactory.facade.impl.*;
import com.fpis.milkfactory.util.CustomResponseEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import javax.annotation.Resource;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/milk-factory")
public class Controller {

    private static final Logger LOGGER = LoggerFactory.getLogger(Controller.class);
    private static final String SAVE_CERTIFICATE_SUCCESS = "Certificate saved successfully!";
    private static final String SAVE_CUSTOMER_RECEIPT_SUCCESS = "Customer receipt saved successfully!";
    private static final String DELETE_CUSTOMER_RECEIPT_SUCCESS = "Customer receipt deleted successfully!";
    private static final String DELETE_CERTIFICATE_SUCCESS = "Quality certificate deleted successfully!";

    @Resource
    private CustomerFacadeImpl customerFacade;

    @Resource
    private QualityRequestFacade qualityRequestFacade;

    @Resource
    private QualityCertificateFacade qualityCertificateFacade;

    @Resource
    private DeliveryNoteFacade deliveryNoteFacade;

    @Resource
    private ProductFacade productFacade;

    @Resource
    private CustomerReceiptFacade customerReceiptFacade;

    @Resource
    private PaymentMethodFacade paymentMethodFacade;

    @Resource
    private ApiObjectValidator apiObjectValidator;

    @Resource
    private WorkerFacade workerFacade;

    @GetMapping("/customers/{customerId}")
    public ResponseEntity<?> getCustomerById(@PathVariable long customerId) {
        try {
            CustomerDTO apiBuilding = customerFacade.getCustomerById(customerId);
            return CustomResponseEntity.success(apiBuilding);
        } catch (MFBusinessException e) {
            LOGGER.warn(e.getMessage(), e);
            return CustomResponseEntity.error(e);
        } catch (RuntimeException ex) {
            LOGGER.error(ex.getMessage(), ex);
            return CustomResponseEntity.error(ex);
        }
    }

    @GetMapping("/requests")
    public ResponseEntity<?> getRequests(@RequestParam(required = false) Long requestId,
                                         @RequestParam(required = false) String date) {
        try {
            List<QualityRequestDTO> qualityRequestsDTO = qualityRequestFacade.getRequests(requestId, date);
            return CustomResponseEntity.success(qualityRequestsDTO);
        } catch (MFBusinessException e) {
            LOGGER.warn(e.getMessage(), e);
            return CustomResponseEntity.error(e);
        } catch (RuntimeException ex) {
            LOGGER.error(ex.getMessage(), ex);
            return CustomResponseEntity.error(ex);
        }
    }

    @PostMapping("/quality-certificates")
    public ResponseEntity<?> saveQualityCertificate(@RequestBody @Validated QualityCertificateDTO qualityCertificateDTO, BindingResult result) {
        ResponseEntity<?> errorMap = apiObjectValidator.mapValidationService(result);
        if (errorMap != null) {
            return errorMap;
        }
        try {
            QualityCertificateDTO savedQualityCertificate = qualityCertificateFacade.saveCertificate(qualityCertificateDTO);
            return CustomResponseEntity.success(savedQualityCertificate);
        } catch (MFBusinessException e) {
            LOGGER.warn(e.getMessage(), e);
            return CustomResponseEntity.error(e);
        } catch (RuntimeException ex) {
            LOGGER.error(ex.getMessage(), ex);
            return CustomResponseEntity.error(ex);
        }
    }

    @GetMapping("/quality-certificates")
    public ResponseEntity<?> getQualityCertificates(@RequestParam(required = false) Long certificateId,
                                                    @RequestParam(required = false) String date) {
        try {
            List<QualityCertificateDTO> qualityCertificates = qualityCertificateFacade.getCertificates(certificateId, date);
            return CustomResponseEntity.success(qualityCertificates);
        } catch (MFBusinessException e) {
            LOGGER.warn(e.getMessage(), e);
            return CustomResponseEntity.error(e);
        } catch (RuntimeException ex) {
            LOGGER.error(ex.getMessage(), ex);
            return CustomResponseEntity.error(ex);
        }
    }

    @PutMapping("/quality-certificates/{certificateId}")
    public ResponseEntity<?> updateQualityCertificate(@PathVariable Long certificateId, @RequestBody @Validated QualityCertificateDTO qualityCertificateDTO, BindingResult result) {
        ResponseEntity<?> errorMap = apiObjectValidator.mapValidationService(result);
        if (errorMap != null) {
            return errorMap;
        }
        try {
            QualityCertificateDTO savedCertificate = qualityCertificateFacade.updateCertificate(certificateId, qualityCertificateDTO);
            return CustomResponseEntity.success(savedCertificate);
        } catch (MFBusinessException e) {
            LOGGER.warn(e.getMessage(), e);
            return CustomResponseEntity.error(e);
        } catch (RuntimeException ex) {
            LOGGER.error(ex.getMessage(), ex);
            return CustomResponseEntity.error(ex);
        }
    }

    @DeleteMapping("/quality-certificates/{certificateId}")
    public ResponseEntity<?> deleteQualityCertificate(@PathVariable Long certificateId) {
        try {
            qualityCertificateFacade.deleteCertificate(certificateId);
            return CustomResponseEntity.success(DELETE_CERTIFICATE_SUCCESS);
        } catch (MFBusinessException e) {
            LOGGER.warn(e.getMessage(), e);
            return CustomResponseEntity.error(e);
        } catch (RuntimeException ex) {
            LOGGER.error(ex.getMessage(), ex);
            return CustomResponseEntity.error(ex);
        }
    }

    @GetMapping("/delivery-notes")
    public ResponseEntity<?> getDeliveryNotes(@RequestParam(required = false) Long noteId,
                                              @RequestParam(required = false) String date) {
        try {
            List<DeliveryNoteDTO> deliveryNotes = deliveryNoteFacade.getDeliveryNotes(noteId, date);
            return CustomResponseEntity.success(deliveryNotes);
        } catch (MFBusinessException e) {
            LOGGER.warn(e.getMessage(), e);
            return CustomResponseEntity.error(e);
        } catch (RuntimeException ex) {
            LOGGER.error(ex.getMessage(), ex);
            return CustomResponseEntity.error(ex);
        }
    }

    @GetMapping("/products")
    public ResponseEntity<?> getProducts(@RequestParam(required = false) Long productId,
                                         @RequestParam(required = false) String name) {
        try {
            List<ProductDTO> productsDTO = productFacade.getProducts(productId, name);
            return CustomResponseEntity.success(productsDTO);
        } catch (MFBusinessException e) {
            LOGGER.warn(e.getMessage(), e);
            return CustomResponseEntity.error(e);
        } catch (RuntimeException ex) {
            LOGGER.error(ex.getMessage(), ex);
            return CustomResponseEntity.error(ex);
        }
    }

    @GetMapping("/payment-methods")
    public ResponseEntity<?> getPaymentMethods() {
        try {
            List<PaymentMethodDTO> paymentMethods = paymentMethodFacade.getPaymentMethods();
            return CustomResponseEntity.success(paymentMethods);
        } catch (MFBusinessException e) {
            LOGGER.warn(e.getMessage(), e);
            return CustomResponseEntity.error(e);
        } catch (RuntimeException ex) {
            LOGGER.error(ex.getMessage(), ex);
            return CustomResponseEntity.error(ex);
        }
    }

    @GetMapping("/workers")
    public ResponseEntity<?> getWorkers() {
        try {
            List<WorkerDTO> workers = workerFacade.getWorkers();
            return CustomResponseEntity.success(workers);
        } catch (MFBusinessException e) {
            LOGGER.warn(e.getMessage(), e);
            return CustomResponseEntity.error(e);
        } catch (RuntimeException ex) {
            LOGGER.error(ex.getMessage(), ex);
            return CustomResponseEntity.error(ex);
        }
    }

    @GetMapping("/customer-receipts/{customerReceiptId}")
    public ResponseEntity<?> getCustomerReceiptById(@PathVariable long customerReceiptId) {
        try {
            CustomerReceiptDTO customerReceiptDTO = customerReceiptFacade.getCustomerReceipt(customerReceiptId);
            return CustomResponseEntity.success(customerReceiptDTO);
        } catch (MFBusinessException e) {
            LOGGER.warn(e.getMessage(), e);
            return CustomResponseEntity.error(e);
        } catch (RuntimeException ex) {
            LOGGER.error(ex.getMessage(), ex);
            return CustomResponseEntity.error(ex);
        }
    }

    @GetMapping("/customer-receipts")
    public ResponseEntity<?> getCustomerReceiptByIdAndDate(@RequestParam(required = false) Long customerReceiptId,
                                                           @RequestParam(required = false) String date) {
        try {
            List<CustomerReceiptDTO> customerReceiptDTOs = customerReceiptFacade.getCustomerReceiptByIdAndDate(customerReceiptId, date);
            return CustomResponseEntity.success(customerReceiptDTOs);
        } catch (MFBusinessException e) {
            LOGGER.warn(e.getMessage(), e);
            return CustomResponseEntity.error(e);
        } catch (RuntimeException ex) {
            LOGGER.error(ex.getMessage(), ex);
            return CustomResponseEntity.error(ex);
        }
    }

    @PostMapping("/customer-receipts")
    public ResponseEntity<?> addNewCustomerReceipt(@RequestBody @Validated CustomerReceiptDTO customerReceiptDTO, BindingResult result) {
        ResponseEntity<?> errorMap = apiObjectValidator.mapValidationService(result);
        if (errorMap != null) {
            return errorMap;
        }
        try {
            CustomerReceiptDTO savedCustomerReceipt = customerReceiptFacade.saveCustomerReceipt(customerReceiptDTO);
            return CustomResponseEntity.success(savedCustomerReceipt);
        } catch (MFBusinessException e) {
            LOGGER.warn(e.getMessage(), e);
            return CustomResponseEntity.error(e);
        } catch (RuntimeException ex) {
            LOGGER.error(ex.getMessage(), ex);
            return CustomResponseEntity.error(ex);
        }
    }

    @PutMapping("/customer-receipts/{customerReceiptId}")
    public ResponseEntity<?> updateCustomerReceipt(@RequestBody @Validated CustomerReceiptDTO customerReceiptDTO, @PathVariable long customerReceiptId, BindingResult result) {
        ResponseEntity<?> errorMap = apiObjectValidator.mapValidationService(result);
        if (errorMap != null) {
            return errorMap;
        }
        try {
            CustomerReceiptDTO savedReceipt = customerReceiptFacade.updateCustomerReceipt(customerReceiptDTO, customerReceiptId);
            return CustomResponseEntity.success(savedReceipt);
        } catch (MFBusinessException e) {
            LOGGER.warn(e.getMessage(), e);
            return CustomResponseEntity.error(e);
        } catch (RuntimeException ex) {
            LOGGER.error(ex.getMessage(), ex);
            return CustomResponseEntity.error(ex);
        }
    }

    @DeleteMapping("/customer-receipts/{customerReceiptId}")
    public ResponseEntity<?> deleteCustomerReceipt(@PathVariable long customerReceiptId) {
        try {
            customerReceiptFacade.deleteCustomerReceipt(customerReceiptId);
            return CustomResponseEntity.success(DELETE_CUSTOMER_RECEIPT_SUCCESS);
        } catch (MFBusinessException e) {
            LOGGER.warn(e.getMessage(), e);
            return CustomResponseEntity.error(e);
        } catch (RuntimeException ex) {
            LOGGER.error(ex.getMessage(), ex);
            return CustomResponseEntity.error(ex);
        }
    }
}
