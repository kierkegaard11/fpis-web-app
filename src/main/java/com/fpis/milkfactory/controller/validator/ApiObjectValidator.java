package com.fpis.milkfactory.controller.validator;

import com.fpis.milkfactory.util.CustomResponseEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.HashMap;
import java.util.Map;

@Component
public class ApiObjectValidator {

    public ResponseEntity<?> mapValidationService(BindingResult result) {

        if (result.hasErrors()) {
            Map<String, String> errorMap = new HashMap<>();

            for (FieldError error : result.getFieldErrors()) {
                errorMap.put(error.getField(), error.getDefaultMessage());
            }
            return CustomResponseEntity.error(errorMap);
        }
        return null;
    }
}
