package com.fpis.milkfactory.dto;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class CustomerReceiptItemDTO extends BaseDTOModel{

    private Long id;
    private Long customerReceiptId;

    @NotNull
    private Long productId;

    @NotNull
    private BigDecimal quantity;

    @NotNull
    private BigDecimal rebate;

    @NotNull
    private BigDecimal priceWithDiscount;

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getRebate() {
        return rebate;
    }

    public void setRebate(BigDecimal rebate) {
        this.rebate = rebate;
    }

    public BigDecimal getPriceWithDiscount() {
        return priceWithDiscount;
    }

    public void setPriceWithDiscount(BigDecimal priceWithDiscount) {
        this.priceWithDiscount = priceWithDiscount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCustomerReceiptId() {
        return customerReceiptId;
    }

    public void setCustomerReceiptId(Long customerReceiptId) {
        this.customerReceiptId = customerReceiptId;
    }
}
