package com.fpis.milkfactory.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

public class CustomerReceiptDTO extends BaseDTOModel{

    private Long id;

    @NotNull
    private String dueDate;

    @Size(max=255, message = "Note must not be longer than 255 characters")
    private String note;

    private List<Long> paymentMethodIds = new ArrayList<>();
    private List<PaymentMethodDTO> paymentMethods = new ArrayList<>();
    private List<CustomerReceiptItemDTO> items = new ArrayList<>();

    @NotNull
    private Long deliveryNoteId;

    private DeliveryNoteDTO deliveryNoteDTO;

    @NotNull
    private Long workerId;

    @Size(min=2, max=45, message = "Name and surname must contain between 2 and 45 characters")
    private String personReceived;

    @NotNull
    private Long customerId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public DeliveryNoteDTO getDeliveryNoteDTO() {
        return deliveryNoteDTO;
    }

    public void setDeliveryNoteDTO(DeliveryNoteDTO deliveryNoteDTO) {
        this.deliveryNoteDTO = deliveryNoteDTO;
    }

    public List<Long> getPaymentMethodIds() {
        return paymentMethodIds;
    }

    public void setPaymentMethodIds(List<Long> paymentMethodIds) {
        this.paymentMethodIds = paymentMethodIds;
    }

    public List<CustomerReceiptItemDTO> getItems() {
        return items;
    }

    public void setItems(List<CustomerReceiptItemDTO> items) {
        this.items = items;
    }

    public Long getDeliveryNoteId() {
        return deliveryNoteId;
    }

    public void setDeliveryNoteId(Long deliveryNoteId) {
        this.deliveryNoteId = deliveryNoteId;
    }

    public Long getWorkerId() {
        return workerId;
    }

    public void setWorkerId(Long workerId) {
        this.workerId = workerId;
    }

    public String getPersonReceived() {
        return personReceived;
    }

    public void setPersonReceived(String personReceived) {
        this.personReceived = personReceived;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public List<PaymentMethodDTO> getPaymentMethods() {
        return paymentMethods;
    }

    public void setPaymentMethods(List<PaymentMethodDTO> paymentMethods) {
        this.paymentMethods = paymentMethods;
    }
}
