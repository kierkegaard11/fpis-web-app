package com.fpis.milkfactory.dto;

import java.time.LocalDateTime;
import java.util.Date;

public class DeliveryNoteDTO extends BaseDTOModel {

    private Long id;
    private Long customerId;
    private Date dispatchDate;
    private Long workerDelivered;
    private Long workerIssued;

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Date getDispatchDate() {
        return dispatchDate;
    }

    public void setDispatchDate(Date dispatchDate) {
        this.dispatchDate = dispatchDate;
    }

    public Long getWorkerDelivered() {
        return workerDelivered;
    }

    public void setWorkerDelivered(Long workerDelivered) {
        this.workerDelivered = workerDelivered;
    }

    public Long getWorkerIssued() {
        return workerIssued;
    }

    public void setWorkerIssued(Long workerIssued) {
        this.workerIssued = workerIssued;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
