package com.fpis.milkfactory.dto;

import com.fpis.milkfactory.persistence.model.Worker;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

public class QualityRequestDTO extends BaseDTOModel{

    private Long id;
    private Date date;
    private String note;
    private Long veterinarianId;
    private Long productId;
    private Long workerId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getVeterinarianId() {
        return veterinarianId;
    }

    public void setVeterinarianId(Long veterinarianId) {
        this.veterinarianId = veterinarianId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getWorkerId() {
        return workerId;
    }

    public void setWorkerId(Long workerId) {
        this.workerId = workerId;
    }
}
