package com.fpis.milkfactory.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class QualityCertificateDTO extends BaseDTOModel {

    private Long id;
    @NotNull
    private Long qualityRequestId;

    @NotNull
    private String date;

    @Size(max=255, message = "Description must not be longer than 255 characters")
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getQualityRequestId() {
        return qualityRequestId;
    }

    public void setQualityRequestId(Long qualityRequestId) {
        this.qualityRequestId = qualityRequestId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
