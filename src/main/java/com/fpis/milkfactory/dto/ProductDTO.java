package com.fpis.milkfactory.dto;

import com.fpis.milkfactory.persistence.model.MeasurementUnit;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

public class ProductDTO extends BaseDTOModel{

    private Long id;
    private String name;
    private MeasurementUnit unit;
    private Date dateCreated;
    private BigDecimal price;
    private Long productTypeId;
    private BigDecimal quantityOnStock;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MeasurementUnit getUnit() {
        return unit;
    }

    public void setUnit(MeasurementUnit unit) {
        this.unit = unit;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Long getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(Long productTypeId) {
        this.productTypeId = productTypeId;
    }

    public BigDecimal getQuantityOnStock() {
        return quantityOnStock;
    }

    public void setQuantityOnStock(BigDecimal quantityOnStock) {
        this.quantityOnStock = quantityOnStock;
    }
}
