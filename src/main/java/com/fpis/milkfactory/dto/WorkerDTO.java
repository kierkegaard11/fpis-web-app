package com.fpis.milkfactory.dto;

import com.fpis.milkfactory.persistence.model.Workplace;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

public class WorkerDTO extends BaseDTOModel {

    private Long workerId;

    private String firstLastName;

    public Long getWorkerId() {
        return workerId;
    }

    public void setWorkerId(Long workerId) {
        this.workerId = workerId;
    }

    public String getFirstLastName() {
        return firstLastName;
    }

    public void setFirstLastName(String firstLastName) {
        this.firstLastName = firstLastName;
    }
}
