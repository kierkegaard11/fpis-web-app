package com.fpis.milkfactory.converter;

import com.fpis.milkfactory.dto.BaseDTOModel;
import com.fpis.milkfactory.dto.QualityRequestDTO;
import com.fpis.milkfactory.persistence.model.BaseDomainModel;
import com.fpis.milkfactory.persistence.model.QualityRequest;
import org.springframework.stereotype.Service;

@Service
public class QualityRequestConverter extends GenericConverter{
    @Override
    protected void convertDomainToDTO(BaseDomainModel source, BaseDTOModel target) {
        QualityRequestDTO qualityRequestDTO = (QualityRequestDTO) target;
        QualityRequest qualityRequest = (QualityRequest) source;
        qualityRequestDTO.setId(qualityRequest.getId());
        qualityRequestDTO.setDate(qualityRequest.getDate());
        qualityRequestDTO.setNote(qualityRequest.getNote());
        qualityRequestDTO.setVeterinarianId(qualityRequest.getVeterinarian().getId());
        qualityRequestDTO.setProductId(qualityRequest.getProduct().getId());
        qualityRequestDTO.setWorkerId(qualityRequest.getWorker().getId());
    }

    @Override
    protected BaseDTOModel createBaseDTOModel() {
        return new QualityRequestDTO();
    }

    @Override
    protected void convertDTOToDomain(BaseDTOModel source, BaseDomainModel target) {

    }

    @Override
    protected BaseDomainModel createBaseDomainModel() {
        return new QualityRequest();
    }
}
