package com.fpis.milkfactory.converter;

import com.fpis.milkfactory.dto.BaseDTOModel;
import com.fpis.milkfactory.dto.PaymentMethodDTO;
import com.fpis.milkfactory.persistence.model.BaseDomainModel;
import com.fpis.milkfactory.persistence.model.PaymentMethod;
import org.springframework.stereotype.Service;

@Service
public class PaymentMethodConverter extends GenericConverter {
    @Override
    protected void convertDomainToDTO(BaseDomainModel source, BaseDTOModel target) {
        PaymentMethod paymentMethod = (PaymentMethod) source;
        PaymentMethodDTO paymentMethodDTO = (PaymentMethodDTO) target;
        paymentMethodDTO.setId(paymentMethod.getId());
        paymentMethodDTO.setName(paymentMethod.getName());
    }

    @Override
    protected BaseDTOModel createBaseDTOModel() {
        return new PaymentMethodDTO();
    }

    @Override
    protected void convertDTOToDomain(BaseDTOModel source, BaseDomainModel target) {
        PaymentMethod paymentMethod = (PaymentMethod) target;
        PaymentMethodDTO paymentMethodDTO = (PaymentMethodDTO) source;
        paymentMethod.setId(paymentMethodDTO.getId());
        paymentMethod.setName(paymentMethod.getName());
    }

    @Override
    protected BaseDomainModel createBaseDomainModel() {
        return new PaymentMethod();
    }
}
