package com.fpis.milkfactory.converter;

import com.fpis.milkfactory.dto.BaseDTOModel;
import com.fpis.milkfactory.dto.QualityCertificateDTO;
import com.fpis.milkfactory.persistence.model.BaseDomainModel;
import com.fpis.milkfactory.persistence.model.QualityCertificate;
import com.fpis.milkfactory.persistence.model.QualityRequest;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;

@Service
public class QualityCertificateConverter extends GenericConverter {
    @Override
    protected void convertDomainToDTO(BaseDomainModel source, BaseDTOModel target) {
        QualityCertificateDTO qualityCertificateDTO = (QualityCertificateDTO) target;
        QualityCertificate qualityCertificate = (QualityCertificate) source;
        if(qualityCertificate.getDate()!=null) {
            qualityCertificateDTO.setDate(qualityCertificate.getDate().toString());
        }
        qualityCertificateDTO.setId(qualityCertificate.getId());
        qualityCertificateDTO.setQualityRequestId(qualityCertificate.getQualityRequest().getId());
        qualityCertificateDTO.setDescription(qualityCertificate.getDescription());
    }

    @Override
    protected BaseDTOModel createBaseDTOModel() {
        return new QualityCertificateDTO();
    }

    @Override
    protected void convertDTOToDomain(BaseDTOModel source, BaseDomainModel target) {
        QualityCertificateDTO qualityCertificateDTO = (QualityCertificateDTO) source;
        QualityCertificate qualityCertificate = (QualityCertificate) target;
        qualityCertificate.setId(qualityCertificateDTO.getId());
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = qualityCertificateDTO.getDate();
        String dateTime = date + " 09:00:00";
        try {
            qualityCertificate.setDate(format.parse(dateTime));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        QualityRequest qualityRequest = new QualityRequest();
        qualityRequest.setId(qualityCertificateDTO.getQualityRequestId());
        qualityCertificate.setQualityRequest(qualityRequest);
        qualityCertificate.setDescription(qualityCertificateDTO.getDescription());
    }

    @Override
    protected BaseDomainModel createBaseDomainModel() {
        return new QualityCertificate();
    }
}
