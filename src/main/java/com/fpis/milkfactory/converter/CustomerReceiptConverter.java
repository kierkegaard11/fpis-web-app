package com.fpis.milkfactory.converter;

import com.fpis.milkfactory.dto.BaseDTOModel;
import com.fpis.milkfactory.dto.CustomerReceiptDTO;
import com.fpis.milkfactory.persistence.model.*;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;

@Service
public class CustomerReceiptConverter extends GenericConverter {


    @Override
    protected void convertDomainToDTO(BaseDomainModel source, BaseDTOModel target) {
        CustomerReceipt customerReceipt = (CustomerReceipt) source;
        CustomerReceiptDTO customerReceiptDTO = (CustomerReceiptDTO) target;
        customerReceiptDTO.setId(customerReceipt.getId());
        if (customerReceipt.getDueDate() != null) {
            customerReceiptDTO.setDueDate(customerReceipt.getDueDate().toString());
        }
        customerReceiptDTO.setNote(customerReceipt.getNote());
        customerReceiptDTO.setDeliveryNoteId(customerReceipt.getDeliveryNote().getId());
        customerReceiptDTO.setWorkerId(customerReceipt.getWorker().getId());
        customerReceiptDTO.setPersonReceived(customerReceipt.getPersonReceived());
        customerReceiptDTO.setCustomerId(customerReceipt.getCustomer().getId());
    }

    @Override
    protected BaseDTOModel createBaseDTOModel() {
        return new CustomerReceiptDTO();
    }

    @Override
    protected void convertDTOToDomain(BaseDTOModel source, BaseDomainModel target) {
        CustomerReceiptDTO customerReceiptDTO = (CustomerReceiptDTO) source;
        CustomerReceipt customerReceipt = (CustomerReceipt) target;
        customerReceipt.setNote(customerReceiptDTO.getNote());
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = customerReceiptDTO.getDueDate();
        String dateTime = date + " 09:00:00";
        try {
            customerReceipt.setDueDate(format.parse(dateTime));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DeliveryNote deliveryNote = new DeliveryNote();
        deliveryNote.setId(customerReceiptDTO.getDeliveryNoteId());
        customerReceipt.setDeliveryNote(deliveryNote);
        Worker worker = new Worker();
        worker.setId(customerReceiptDTO.getWorkerId());
        customerReceipt.setWorker(worker);
        customerReceipt.setPersonReceived(customerReceiptDTO.getPersonReceived());
        Customer customer = new Customer();
        customer.setId(customerReceiptDTO.getCustomerId());
        customerReceipt.setCustomer(customer);
    }

    @Override
    protected BaseDomainModel createBaseDomainModel() {
        return new CustomerReceipt();
    }
}
