package com.fpis.milkfactory.converter;

import com.fpis.milkfactory.dto.BaseDTOModel;
import com.fpis.milkfactory.dto.WorkerDTO;
import com.fpis.milkfactory.persistence.model.BaseDomainModel;
import com.fpis.milkfactory.persistence.model.Worker;
import org.springframework.stereotype.Component;

@Component
public class WorkerConverter extends GenericConverter {
    @Override
    protected void convertDomainToDTO(BaseDomainModel source, BaseDTOModel target) {
        Worker worker = (Worker) source;
        WorkerDTO workerDTO = (WorkerDTO) target;

        workerDTO.setWorkerId(worker.getId());
        workerDTO.setFirstLastName(worker.getFirstLastName());
    }

    @Override
    protected BaseDTOModel createBaseDTOModel() {
        return new WorkerDTO();
    }

    @Override
    protected void convertDTOToDomain(BaseDTOModel source, BaseDomainModel target) {

    }

    @Override
    protected BaseDomainModel createBaseDomainModel() {
        return null;
    }
}
