package com.fpis.milkfactory.converter;

import com.fpis.milkfactory.dto.BaseDTOModel;
import com.fpis.milkfactory.dto.DeliveryNoteDTO;
import com.fpis.milkfactory.persistence.model.BaseDomainModel;
import com.fpis.milkfactory.persistence.model.DeliveryNote;
import org.springframework.stereotype.Service;

@Service
public class DeliveryNoteConverter extends GenericConverter {
    @Override
    protected void convertDomainToDTO(BaseDomainModel source, BaseDTOModel target) {
        DeliveryNote deliveryNote = (DeliveryNote) source;
        DeliveryNoteDTO deliveryNoteDTO = (DeliveryNoteDTO) target;
        deliveryNoteDTO.setId(deliveryNote.getId());
        deliveryNoteDTO.setCustomerId(deliveryNote.getCustomer().getId());
        deliveryNoteDTO.setDispatchDate(deliveryNote.getDispatchDate());
        deliveryNoteDTO.setWorkerDelivered(deliveryNote.getWorkerDelivered().getId());
        deliveryNoteDTO.setWorkerIssued(deliveryNote.getWorkerIssued().getId());
    }

    @Override
    protected BaseDTOModel createBaseDTOModel() {
        return new DeliveryNoteDTO();
    }

    @Override
    protected void convertDTOToDomain(BaseDTOModel source, BaseDomainModel target) {

    }

    @Override
    protected BaseDomainModel createBaseDomainModel() {
        return new DeliveryNote();
    }
}
