package com.fpis.milkfactory.converter;

import com.fpis.milkfactory.dto.BaseDTOModel;
import com.fpis.milkfactory.dto.CustomerDTO;
import com.fpis.milkfactory.persistence.model.BaseDomainModel;
import com.fpis.milkfactory.persistence.model.Customer;
import org.springframework.stereotype.Service;

@Service
public class CustomerConverter extends GenericConverter {
    @Override
    protected void convertDomainToDTO(BaseDomainModel source, BaseDTOModel target) {
        Customer customer = (Customer) source;
        CustomerDTO customerDTO = (CustomerDTO) target;
        customerDTO.setCompanyId(customer.getCompanyId());
        customerDTO.setCompanyNumber(customer.getCompanyNumber());
        customerDTO.setEmail(customer.getEmail());
        customerDTO.setName(customer.getName());
        customerDTO.setBankAccountNumber(customer.getBankAccountNumber());
        customerDTO.setWebsite(customer.getWebsite());
    }

    @Override
    protected BaseDTOModel createBaseDTOModel() {
        return new CustomerDTO();
    }

    @Override
    protected void convertDTOToDomain(BaseDTOModel source, BaseDomainModel target) {

    }

    @Override
    protected BaseDomainModel createBaseDomainModel() {
        return new Customer();
    }
}
