package com.fpis.milkfactory.converter;

import com.fpis.milkfactory.dto.BaseDTOModel;
import com.fpis.milkfactory.persistence.model.BaseDomainModel;

public abstract class GenericConverter {

    public BaseDTOModel convertDomainToDTO(BaseDomainModel source) {
        BaseDTOModel target = createBaseDTOModel();
        convertDomainToDTO(source, target);
        return target;
    }

    public BaseDomainModel convertDTOToDomain(BaseDTOModel source) {
        BaseDomainModel target = createBaseDomainModel();
        convertDTOToDomain(source, target);
        return target;
    }

    protected abstract void convertDomainToDTO(BaseDomainModel source, BaseDTOModel target);

    protected abstract BaseDTOModel createBaseDTOModel();

    protected abstract void convertDTOToDomain(BaseDTOModel source, BaseDomainModel target);

    protected abstract BaseDomainModel createBaseDomainModel();
}
