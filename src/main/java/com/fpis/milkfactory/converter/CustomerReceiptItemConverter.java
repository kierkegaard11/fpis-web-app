package com.fpis.milkfactory.converter;

import com.fpis.milkfactory.dto.BaseDTOModel;
import com.fpis.milkfactory.dto.CustomerReceiptItemDTO;
import com.fpis.milkfactory.persistence.model.BaseDomainModel;
import com.fpis.milkfactory.persistence.model.CustomerReceiptItem;
import com.fpis.milkfactory.persistence.model.Product;
import org.springframework.stereotype.Service;

@Service
public class CustomerReceiptItemConverter extends GenericConverter {
    @Override
    protected void convertDomainToDTO(BaseDomainModel source, BaseDTOModel target) {
        CustomerReceiptItem customerReceiptItem = (CustomerReceiptItem) source;
        CustomerReceiptItemDTO customerReceiptItemDTO = (CustomerReceiptItemDTO) target;
        customerReceiptItemDTO.setProductId(customerReceiptItem.getProduct().getId());
        customerReceiptItemDTO.setRebate(customerReceiptItem.getRebate());
        customerReceiptItemDTO.setQuantity(customerReceiptItem.getQuantity());
        customerReceiptItemDTO.setPriceWithDiscount(customerReceiptItem.getPriceWithDiscount());
        customerReceiptItemDTO.setId(customerReceiptItem.getId());
        customerReceiptItemDTO.setCustomerReceiptId(customerReceiptItem.getCustomerReceipt().getId());
    }

    @Override
    protected BaseDTOModel createBaseDTOModel() {
        return new CustomerReceiptItemDTO();
    }

    @Override
    protected void convertDTOToDomain(BaseDTOModel source, BaseDomainModel target) {
        CustomerReceiptItemDTO customerReceiptItemDTO = (CustomerReceiptItemDTO) source;
        CustomerReceiptItem customerReceiptItem = (CustomerReceiptItem) target;
        customerReceiptItem.setId(customerReceiptItemDTO.getId());
        customerReceiptItem.setRebate(customerReceiptItemDTO.getRebate());
        customerReceiptItem.setQuantity(customerReceiptItemDTO.getQuantity());
        customerReceiptItem.setPriceWithDiscount(customerReceiptItemDTO.getPriceWithDiscount());
        Product product = new Product();
        product.setId(customerReceiptItemDTO.getProductId());
        customerReceiptItem.setProduct(product);
    }

    @Override
    protected BaseDomainModel createBaseDomainModel() {
        return new CustomerReceiptItem();
    }
}
