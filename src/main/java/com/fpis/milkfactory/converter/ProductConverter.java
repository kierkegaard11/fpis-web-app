package com.fpis.milkfactory.converter;

import com.fpis.milkfactory.dto.BaseDTOModel;
import com.fpis.milkfactory.dto.ProductDTO;
import com.fpis.milkfactory.persistence.model.BaseDomainModel;
import com.fpis.milkfactory.persistence.model.Product;
import org.springframework.stereotype.Service;

@Service
public class ProductConverter extends GenericConverter {

    @Override
    protected void convertDomainToDTO(BaseDomainModel source, BaseDTOModel target) {
        Product product = (Product) source;
        ProductDTO productDTO = (ProductDTO) target;
        productDTO.setId(product.getId());
        productDTO.setName(product.getName());
        productDTO.setUnit(product.getUnit());
        productDTO.setDateCreated(product.getDateCreated());
        productDTO.setProductTypeId(product.getProductType().getId());
        productDTO.setPrice(product.getPrice());
        productDTO.setQuantityOnStock(product.getQuantityOnStock());
    }

    @Override
    protected BaseDTOModel createBaseDTOModel() {
        return new ProductDTO();
    }

    @Override
    protected void convertDTOToDomain(BaseDTOModel source, BaseDomainModel target) {

    }

    @Override
    protected BaseDomainModel createBaseDomainModel() {
        return new Product();
    }
}
