package com.fpis.milkfactory.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "mfEntityManagerFactory",
        transactionManagerRef = "mfTransactionManager", basePackages = {"com.fpis.milkfactory.persistence.dao"})
public class MilkFactoryDataSourceConfiguration {

    @Bean(name = "mfDataSourceProperties")
    @ConfigurationProperties("spring.maindatasource")
    public DataSourceProperties mainDataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean(name = "mfDataSource")
    @ConditionalOnMissingBean(name = "mfDataSource")
    @ConfigurationProperties(prefix = "spring.maindatasource")
    public DataSource dataSource(@Qualifier("mfDataSourceProperties") DataSourceProperties mainDataSourceProperties) {
        return mainDataSourceProperties.initializeDataSourceBuilder().build();
    }

    @Bean(name = "mfEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean mainEntityManagerFactory(EntityManagerFactoryBuilder builder,
                                                                           @Qualifier("mfDataSource") DataSource mainDataSource) {
        return builder.dataSource(mainDataSource).packages("com.fpis.milkfactory.persistence.model")
                .persistenceUnit("mainPU").build();
    }

    @Bean(name = "mfTransactionManager")
    public PlatformTransactionManager mainTransactionManager(
            @Qualifier("mfEntityManagerFactory") EntityManagerFactory mainEntityManagerFactory) {
        return new JpaTransactionManager(mainEntityManagerFactory);
    }
}
