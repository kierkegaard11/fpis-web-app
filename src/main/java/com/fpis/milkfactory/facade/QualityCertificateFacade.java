package com.fpis.milkfactory.facade;

import com.fpis.milkfactory.dto.QualityCertificateDTO;
import com.fpis.milkfactory.persistence.model.QualityCertificate;

import java.util.List;

public interface QualityCertificateFacade{

    QualityCertificateDTO saveCertificate(QualityCertificateDTO qualityCertificateDTO);

    List<QualityCertificateDTO> getCertificates(Long certificateId, String date);

    QualityCertificateDTO updateCertificate(Long certificateId, QualityCertificateDTO qualityCertificateDTO);

    void deleteCertificate(Long certificateId);
}
