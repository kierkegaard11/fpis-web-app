package com.fpis.milkfactory.facade;

import com.fpis.milkfactory.dto.PaymentMethodDTO;

import java.util.List;

public interface PaymentMethodFacade {

    public List<PaymentMethodDTO> getPaymentMethods();
}
