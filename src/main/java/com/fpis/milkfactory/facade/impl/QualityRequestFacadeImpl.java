package com.fpis.milkfactory.facade.impl;

import com.fpis.milkfactory.converter.QualityRequestConverter;
import com.fpis.milkfactory.dto.QualityRequestDTO;
import com.fpis.milkfactory.facade.QualityRequestFacade;
import com.fpis.milkfactory.persistence.model.QualityRequest;
import com.fpis.milkfactory.service.QualityRequestService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class QualityRequestFacadeImpl implements QualityRequestFacade {

    @Resource
    private QualityRequestService qualityRequestService;
    @Resource
    private QualityRequestConverter qualityRequestConverter;

    @Override
    public List<QualityRequestDTO> getRequests(Long requestId, String date) {
        List<QualityRequest> requests = qualityRequestService.getRequests(requestId, date);
        List<QualityRequestDTO> requestsDTO = new ArrayList<>();
        requests.forEach(qualityRequest -> {
            QualityRequestDTO qualityRequestDTO = (QualityRequestDTO) qualityRequestConverter.convertDomainToDTO(qualityRequest);
            requestsDTO.add(qualityRequestDTO);
        });
        return requestsDTO;
    }
}
