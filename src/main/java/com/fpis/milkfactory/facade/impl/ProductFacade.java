package com.fpis.milkfactory.facade.impl;

import com.fpis.milkfactory.converter.ProductConverter;
import com.fpis.milkfactory.dto.DeliveryNoteDTO;
import com.fpis.milkfactory.dto.ProductDTO;
import com.fpis.milkfactory.persistence.model.DeliveryNote;
import com.fpis.milkfactory.persistence.model.Product;
import com.fpis.milkfactory.service.ProductService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class ProductFacade {

    @Resource
    private ProductService productService;

    @Resource
    private ProductConverter productConverter;

    public List<ProductDTO> getProducts(Long productId, String name) {
        List<Product> products = productService.getProducts(productId, name);
        List<ProductDTO> productsDTO = new ArrayList<>();
        products.forEach(product -> {
            productsDTO.add((ProductDTO) productConverter.convertDomainToDTO(product));
        });
        return productsDTO;
    }

}
