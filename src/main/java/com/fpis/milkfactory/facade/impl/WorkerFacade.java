package com.fpis.milkfactory.facade.impl;

import com.fpis.milkfactory.converter.WorkerConverter;
import com.fpis.milkfactory.dto.WorkerDTO;
import com.fpis.milkfactory.persistence.model.Worker;
import com.fpis.milkfactory.service.WorkerService;
import org.hibernate.jdbc.Work;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class WorkerFacade {

    @Resource
    private WorkerConverter workerConverter;
    @Resource
    private WorkerService workerService;

    public List<WorkerDTO> getWorkers() {
        List<Worker> workers = workerService.getWorkers();
        List<WorkerDTO> workerDTOS = new ArrayList<>();
        for (Worker worker : workers) {
            workerDTOS.add((WorkerDTO) workerConverter.convertDomainToDTO(worker));
        }
        return workerDTOS;
    }
}
