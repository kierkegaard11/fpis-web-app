package com.fpis.milkfactory.facade.impl;

import com.fpis.milkfactory.converter.CustomerReceiptConverter;
import com.fpis.milkfactory.converter.CustomerReceiptItemConverter;
import com.fpis.milkfactory.converter.DeliveryNoteConverter;
import com.fpis.milkfactory.converter.PaymentMethodConverter;
import com.fpis.milkfactory.dto.CustomerReceiptDTO;
import com.fpis.milkfactory.dto.CustomerReceiptItemDTO;
import com.fpis.milkfactory.dto.DeliveryNoteDTO;
import com.fpis.milkfactory.dto.PaymentMethodDTO;
import com.fpis.milkfactory.persistence.model.*;
import com.fpis.milkfactory.service.*;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import java.util.*;

@Service
public class CustomerReceiptFacade {

    @Resource
    private CustomerReceiptConverter customerReceiptConverter;
    @Resource
    private CustomerReceiptItemConverter customerReceiptItemConverter;
    @Resource
    private CustomerReceiptItemService customerReceiptItemService;
    @Resource
    private CustomerReceiptService customerReceiptService;
    @Resource
    private CustomerReceiptPaymentMethodAggrService customerReceiptPaymentMethodAggrService;
    @Resource
    private PaymentMethodConverter paymentMethodConverter;
    @Resource
    private PaymentMethodService paymentMethodService;
    @Resource
    private DeliveryNoteService deliveryNoteService;
    @Resource
    private DeliveryNoteConverter deliveryNoteConverter;

    @Transactional
    public CustomerReceiptDTO saveCustomerReceipt(CustomerReceiptDTO customerReceiptDTO) {
        List<CustomerReceiptItem> customerReceiptItems = new ArrayList<>();

        CustomerReceipt customerReceipt = customerReceiptService
                .saveCustomerReceipt((CustomerReceipt) customerReceiptConverter.convertDTOToDomain(customerReceiptDTO));

        saveCustomerReceiptItems(customerReceiptDTO.getItems(), customerReceipt, customerReceiptItems);
        savePaymentMethodAggr(customerReceiptDTO.getPaymentMethodIds(), customerReceipt);
//        List<CustomerReceiptItemDTO> customerReceiptItemsDTO = new ArrayList<>();
//        customerReceiptItems.forEach(customerReceiptItem -> {
//            customerReceiptItemsDTO.add((CustomerReceiptItemDTO) customerReceiptItemConverter.convertDomainToDTO(customerReceiptItem));
//        });

        return populateCustomerReceiptDTO(customerReceipt);
    }

    private void savePaymentMethodAggr(List<Long> paymentMethods, CustomerReceipt customerReceipt) {
        paymentMethods.forEach(pmId -> {
            PaymentMethod paymentMethod = new PaymentMethod();
            paymentMethod.setId(pmId);
            CustomerReceiptPaymentMethodAggr customerReceiptPaymentMethodAggr = new CustomerReceiptPaymentMethodAggr();
            customerReceiptPaymentMethodAggr.setCustomerReceipt(customerReceipt);
            customerReceiptPaymentMethodAggr.setPaymentMethod(paymentMethod);
            customerReceiptPaymentMethodAggrService.saveCustomerReceiptPaymentmethodAggr(customerReceiptPaymentMethodAggr);
        });
    }

    private void saveCustomerReceiptItems(List<CustomerReceiptItemDTO> customerReceiptItemsDTO, CustomerReceipt savedCustomerReceipt, List<CustomerReceiptItem> customerReceiptItems) {
        customerReceiptItemsDTO.forEach(customerReceiptItemDTO -> {
            CustomerReceiptItem customerReceiptItem = (CustomerReceiptItem) customerReceiptItemConverter
                    .convertDTOToDomain(customerReceiptItemDTO);
            customerReceiptItem.setCustomerReceipt(savedCustomerReceipt);
            CustomerReceiptItem savedCustomerReceiptItem = customerReceiptItemService
                    .saveCustomerReceiptItem(customerReceiptItem);
            customerReceiptItems.add(savedCustomerReceiptItem);
        });
    }

    private CustomerReceiptDTO setItemsAndConvertToDTO(CustomerReceipt customerReceipt, List<CustomerReceiptItemDTO> customerReceiptItemsDTO) {
        CustomerReceiptDTO customerReceiptDTO = (CustomerReceiptDTO) customerReceiptConverter.convertDomainToDTO(customerReceipt);
        customerReceiptDTO.setItems(customerReceiptItemsDTO);
        return customerReceiptDTO;
    }

    @Transactional
    public CustomerReceiptDTO updateCustomerReceipt(CustomerReceiptDTO customerReceiptDTO, long customerReceiptId) {
        CustomerReceipt customerReceipt = (CustomerReceipt) customerReceiptConverter.convertDTOToDomain(customerReceiptDTO);
        customerReceipt.setId(customerReceiptId);
        CustomerReceipt updatedReceipt = customerReceiptService.saveCustomerReceipt(customerReceipt);
        List<CustomerReceiptItemDTO> customerReceiptItemDTOs = dealWithCustomerReceiptItems(customerReceiptDTO.getItems(), updatedReceipt);
        List<PaymentMethodDTO> paymentMethodDTOS = dealWithPaymentMethods(customerReceiptDTO.getPaymentMethodIds(), updatedReceipt);
        CustomerReceiptDTO updatedCustomerReceiptDTO = (CustomerReceiptDTO) customerReceiptConverter.convertDomainToDTO(updatedReceipt);
        updatedCustomerReceiptDTO.setItems(customerReceiptItemDTOs);
        updatedCustomerReceiptDTO.setPaymentMethods(paymentMethodDTOS);
        return updatedCustomerReceiptDTO;
    }

    @Transactional
    public void deleteCustomerReceipt(long customerReceiptId) {
        List<CustomerReceiptPaymentMethodAggr> oldPaymentMethods = customerReceiptPaymentMethodAggrService.getPaymentMethodsByReceiptId(customerReceiptId);
        List<CustomerReceiptItem> oldCustomerReceiptItems = customerReceiptItemService.getItemsByReceiptId(customerReceiptId);

        for (CustomerReceiptPaymentMethodAggr crpm : oldPaymentMethods) {
            customerReceiptPaymentMethodAggrService.deletecustomerReceiptPaymentMethodAggr(crpm);
        }
        for (CustomerReceiptItem customerReceiptItem : oldCustomerReceiptItems) {
            customerReceiptItemService.deleteCustomerReceiptItem(customerReceiptItem);
        }
        CustomerReceipt customerReceipt = new CustomerReceipt();
        customerReceipt.setId(customerReceiptId);
        customerReceiptService.deleteCustomerReceipt(customerReceipt);
    }

    private List<PaymentMethodDTO> dealWithPaymentMethods(List<Long> methodIds, CustomerReceipt updatedReceipt) {
        List<CustomerReceiptPaymentMethodAggr> customerReceiptPaymentMethodAggrs = new ArrayList<>();
        List<PaymentMethod> oldPaymentMethods = getOldPaymentMethods(updatedReceipt, customerReceiptPaymentMethodAggrs);
        List<PaymentMethod> newPaymentMethods = getNewPaymentMethods(methodIds);
        List<PaymentMethod> paymentMethodsToDelete = new ArrayList<>();

        for (PaymentMethod oldPaymentMethod : oldPaymentMethods) {
            if (!newPaymentMethods.contains(oldPaymentMethod)) {
                paymentMethodsToDelete.add(oldPaymentMethod);
            }
        }

        for (PaymentMethod newPaymentMethod : newPaymentMethods) {
            if (!oldPaymentMethods.contains(newPaymentMethod)) {
                savePaymentMethod(newPaymentMethod, updatedReceipt);
            }
        }
        deletePaymentMethods(paymentMethodsToDelete, customerReceiptPaymentMethodAggrs);

        List<CustomerReceiptPaymentMethodAggr> updatedPaymentMethods = customerReceiptPaymentMethodAggrService.getPaymentMethodsByReceiptId(updatedReceipt.getId());
        List<PaymentMethodDTO> updatedPaymentMethodDTOs = new ArrayList<>();
        for (CustomerReceiptPaymentMethodAggr crpm : updatedPaymentMethods) {
            updatedPaymentMethodDTOs.add((PaymentMethodDTO) paymentMethodConverter.convertDomainToDTO(crpm.getPaymentMethod()));
        }

        return updatedPaymentMethodDTOs;
    }

    private List<PaymentMethod> getNewPaymentMethods(List<Long> methodIds) {
        List<PaymentMethod> newPaymentMethods = new ArrayList<>();
        for (Long id : methodIds) {
            newPaymentMethods.add(paymentMethodService.getPaymentMethodById(id));
        }
        return newPaymentMethods;
    }

    private List<PaymentMethod> getOldPaymentMethods(CustomerReceipt updatedReceipt, List<CustomerReceiptPaymentMethodAggr> customerReceiptPaymentMethodAggrs) {
        customerReceiptPaymentMethodAggrs = customerReceiptPaymentMethodAggrService.getPaymentMethodsByReceiptId(updatedReceipt.getId());
        List<PaymentMethod> oldPaymentMethods = new ArrayList<>();
        for (CustomerReceiptPaymentMethodAggr crpm : customerReceiptPaymentMethodAggrs) {
            oldPaymentMethods.add(crpm.getPaymentMethod());
        }
        return oldPaymentMethods;
    }

    private void savePaymentMethod(PaymentMethod newPaymentMethod, CustomerReceipt updatedReceipt) {
        CustomerReceiptPaymentMethodAggr customerReceiptPaymentMethodAggr = new CustomerReceiptPaymentMethodAggr();
        customerReceiptPaymentMethodAggr.setCustomerReceipt(updatedReceipt);
        customerReceiptPaymentMethodAggr.setPaymentMethod(newPaymentMethod);
        customerReceiptPaymentMethodAggrService.saveCustomerReceiptPaymentmethodAggr(customerReceiptPaymentMethodAggr);
    }

    private void deletePaymentMethods(List<PaymentMethod> paymentMethodsToDelete, List<CustomerReceiptPaymentMethodAggr> customerReceiptPaymentMethodAggrs) {
        for (CustomerReceiptPaymentMethodAggr crpm : customerReceiptPaymentMethodAggrs) {
            if (paymentMethodsToDelete.contains(crpm.getPaymentMethod())) {
                customerReceiptPaymentMethodAggrService.deletecustomerReceiptPaymentMethodAggr(crpm);
            }
        }
    }

    private List<CustomerReceiptItemDTO> dealWithCustomerReceiptItems(List<CustomerReceiptItemDTO> newItemsDTO, CustomerReceipt customerReceipt) {
        List<CustomerReceiptItem> oldItems = customerReceiptItemService.getItemsByReceiptId(customerReceipt.getId());
        List<CustomerReceiptItem> newItems = new ArrayList<>();
        List<CustomerReceiptItem> itemsToDelete = new ArrayList<>();
        for (CustomerReceiptItemDTO customerReceiptItemDTO : newItemsDTO) {
            CustomerReceiptItem newItem = (CustomerReceiptItem) customerReceiptItemConverter.convertDTOToDomain(customerReceiptItemDTO);
            newItem.setCustomerReceipt(customerReceipt);
            newItems.add(newItem);
        }
        for (CustomerReceiptItem oldItem : oldItems) {
            if (!newItems.contains(oldItem))
                itemsToDelete.add(oldItem);
        }
        for (CustomerReceiptItem newItem : newItems) {
            if (newItem.getId() == null || oldItems.contains(newItem)) {
                customerReceiptItemService.saveCustomerReceiptItem(newItem);
            }
        }
        for (CustomerReceiptItem itemToDelete : itemsToDelete) {
            customerReceiptItemService.deleteCustomerReceiptItem(itemToDelete);
        }
        List<CustomerReceiptItem> updatedItems = customerReceiptItemService.getItemsByReceiptId(customerReceipt.getId());
        List<CustomerReceiptItemDTO> updatedItemDTOs = new ArrayList<>();
        for (CustomerReceiptItem updatedItem : updatedItems) {
            updatedItemDTOs.add((CustomerReceiptItemDTO) customerReceiptItemConverter.convertDomainToDTO(updatedItem));
        }
        return updatedItemDTOs;
    }

    public CustomerReceiptDTO getCustomerReceipt(long customerReceiptId) {
        CustomerReceipt customerReceipt = customerReceiptService.getCustomerReceiptById(customerReceiptId);
        CustomerReceiptDTO customerReceiptDTO = populateCustomerReceiptDTO(customerReceipt);
        return customerReceiptDTO;
    }

    public List<CustomerReceiptDTO> getCustomerReceiptByIdAndDate(Long customerReceiptId, String date) {
        List<CustomerReceipt> customerReceipts = customerReceiptService.getCustomerReceiptsByIdAndDate(customerReceiptId, date);
        List<CustomerReceiptDTO> customerReceiptDTOs = new ArrayList<>();
        for (CustomerReceipt customerReceipt : customerReceipts) {
            CustomerReceiptDTO customerReceiptDTO = populateCustomerReceiptDTO(customerReceipt);
            customerReceiptDTOs.add(customerReceiptDTO);
        }
        return customerReceiptDTOs;
    }

    private CustomerReceiptDTO populateCustomerReceiptDTO(CustomerReceipt customerReceipt) {
        CustomerReceiptDTO customerReceiptDTO = (CustomerReceiptDTO) customerReceiptConverter.convertDomainToDTO(customerReceipt);
        List<CustomerReceiptPaymentMethodAggr> customerReceiptPaymentMethodAggrs = customerReceiptPaymentMethodAggrService.getPaymentMethodsByReceiptId(customerReceipt.getId());
        List<PaymentMethod> paymentMethods = getOldPaymentMethods(customerReceipt, customerReceiptPaymentMethodAggrs);
        List<CustomerReceiptItem> items = customerReceiptItemService.getItemsByReceiptId(customerReceipt.getId());
        List<CustomerReceiptItemDTO> itemsDTO = new ArrayList<>();
        for (CustomerReceiptItem item : items) {
            itemsDTO.add((CustomerReceiptItemDTO) customerReceiptItemConverter.convertDomainToDTO(item));
        }
        List<PaymentMethodDTO> paymentMethodDTOS = new ArrayList<>();
        for (PaymentMethod paymentMethod : paymentMethods) {
            paymentMethodDTOS.add((PaymentMethodDTO) paymentMethodConverter.convertDomainToDTO(paymentMethod));
        }
        DeliveryNote deliveryNote = deliveryNoteService.getDeliveryNoteById(customerReceipt.getDeliveryNote().getId());
        DeliveryNoteDTO deliveryNoteDTO = (DeliveryNoteDTO) deliveryNoteConverter.convertDomainToDTO(deliveryNote);
        customerReceiptDTO.setDeliveryNoteDTO(deliveryNoteDTO);
        customerReceiptDTO.setItems(itemsDTO);
        customerReceiptDTO.setPaymentMethods(paymentMethodDTOS);
        return customerReceiptDTO;
    }
}
