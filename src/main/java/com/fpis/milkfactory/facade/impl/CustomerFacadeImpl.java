package com.fpis.milkfactory.facade.impl;

import com.fpis.milkfactory.converter.CustomerConverter;
import com.fpis.milkfactory.dto.CustomerDTO;
import com.fpis.milkfactory.facade.CustomerFacade;
import com.fpis.milkfactory.persistence.model.Customer;
import com.fpis.milkfactory.service.CustomerService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class CustomerFacadeImpl implements CustomerFacade {

    @Resource
    private CustomerService customerService;
    @Resource
    private CustomerConverter customerConverter;

    @Override
    public CustomerDTO getCustomerById(Long id) {
        Customer customer = customerService.getCustomerById(id);
        return (CustomerDTO) customerConverter.convertDomainToDTO(customer);
    }
}
