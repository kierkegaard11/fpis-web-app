package com.fpis.milkfactory.facade.impl;

import com.fpis.milkfactory.converter.QualityCertificateConverter;
import com.fpis.milkfactory.dto.QualityCertificateDTO;
import com.fpis.milkfactory.facade.QualityCertificateFacade;
import com.fpis.milkfactory.persistence.model.QualityCertificate;
import com.fpis.milkfactory.persistence.model.QualityRequest;
import com.fpis.milkfactory.service.QualityCertificateService;
import com.fpis.milkfactory.service.QualityRequestService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class QualityCertificateFacadeImpl implements QualityCertificateFacade {

    @Resource
    private QualityCertificateConverter qualityCertificateConverter;
    @Resource
    private QualityCertificateService qualityCertificateService;
    @Resource
    private QualityRequestService qualityRequestService;

    @Override
    public QualityCertificateDTO saveCertificate(QualityCertificateDTO qualityCertificateDTO) {
        QualityCertificate qualityCertificate =
                (QualityCertificate) qualityCertificateConverter.convertDTOToDomain(qualityCertificateDTO);
        QualityRequest qualityRequest =
                qualityRequestService.getQualityRequestById(qualityCertificate.getQualityRequest().getId());
        qualityCertificate.setProduct(qualityRequest.getProduct());
        qualityCertificate.setVeterinarian(qualityRequest.getVeterinarian());
        QualityCertificate savedQualityCertificate = qualityCertificateService.saveQualityCertificate(qualityCertificate);
        return (QualityCertificateDTO) qualityCertificateConverter.convertDomainToDTO(savedQualityCertificate);
    }

    @Override
    public List<QualityCertificateDTO> getCertificates(Long certificateId, String date) {
        List<QualityCertificate> certificates = qualityCertificateService.getCertificates(certificateId, date);
        List<QualityCertificateDTO> certificatesDTO = new ArrayList<>();
        certificates.forEach(certificate -> {
            QualityCertificateDTO qualityRequestDTO =
                    (QualityCertificateDTO) qualityCertificateConverter.convertDomainToDTO(certificate);
            certificatesDTO.add(qualityRequestDTO);
        });
        return certificatesDTO;
    }

    @Override
    public QualityCertificateDTO updateCertificate(Long certificateId, QualityCertificateDTO qualityCertificateDTO) {
        QualityCertificate qualityCertificate =
                (QualityCertificate) qualityCertificateConverter.convertDTOToDomain(qualityCertificateDTO);
        qualityCertificate.setId(certificateId);
        QualityRequest qualityRequest =
                qualityRequestService.getQualityRequestById(qualityCertificate.getQualityRequest().getId());
        qualityCertificate.setProduct(qualityRequest.getProduct());
        qualityCertificate.setVeterinarian(qualityRequest.getVeterinarian());
        QualityCertificate savedCertificate =
                qualityCertificateService.updateQualityCertificate(certificateId, qualityCertificate);
        return (QualityCertificateDTO) qualityCertificateConverter.convertDomainToDTO(savedCertificate);
    }

    @Override
    public void deleteCertificate(Long certificateId) {
        QualityCertificate qualityCertificate = new QualityCertificate();
        qualityCertificate.setId(certificateId);
        qualityCertificateService.deleteQualityCertificate(qualityCertificate);
    }
}
