package com.fpis.milkfactory.facade.impl;

import com.fpis.milkfactory.converter.PaymentMethodConverter;
import com.fpis.milkfactory.dto.PaymentMethodDTO;
import com.fpis.milkfactory.facade.PaymentMethodFacade;
import com.fpis.milkfactory.persistence.model.PaymentMethod;
import com.fpis.milkfactory.service.PaymentMethodService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class PaymentMethodFacadeImpl implements PaymentMethodFacade {
    @Resource
    private PaymentMethodService paymentMethodService;
    @Resource
    private PaymentMethodConverter paymentMethodConverter;

    @Override
    public List<PaymentMethodDTO> getPaymentMethods() {
        List<PaymentMethod> paymentMethods = paymentMethodService.getPaymentMethods();
        List<PaymentMethodDTO> paymentMethodDTOs = new ArrayList<>();
        for (PaymentMethod paymentMethod : paymentMethods) {
            paymentMethodDTOs.add((PaymentMethodDTO) paymentMethodConverter.convertDomainToDTO(paymentMethod));
        }
        return paymentMethodDTOs;
    }
}
