package com.fpis.milkfactory.facade.impl;

import com.fpis.milkfactory.converter.DeliveryNoteConverter;
import com.fpis.milkfactory.dto.DeliveryNoteDTO;
import com.fpis.milkfactory.persistence.model.DeliveryNote;
import com.fpis.milkfactory.service.DeliveryNoteService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class DeliveryNoteFacade {

    @Resource
    private DeliveryNoteService deliveryNoteService;
    @Resource
    private DeliveryNoteConverter deliveryNoteConverter;

    public List<DeliveryNoteDTO> getDeliveryNotes(Long noteId, String date) {
        List<DeliveryNote> deliveryNotes = deliveryNoteService.getDeliveryNotes(noteId, date);
        List<DeliveryNoteDTO> deliveryNotesDTO = new ArrayList<>();
        deliveryNotes.forEach(deliveryNote -> {
            deliveryNotesDTO.add((DeliveryNoteDTO) deliveryNoteConverter.convertDomainToDTO(deliveryNote));
        });
        return deliveryNotesDTO;
    }
}
