package com.fpis.milkfactory.facade;

import com.fpis.milkfactory.dto.CustomerDTO;

public interface CustomerFacade {

    CustomerDTO getCustomerById(Long id);
}
