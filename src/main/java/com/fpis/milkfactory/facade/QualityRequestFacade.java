package com.fpis.milkfactory.facade;

import com.fpis.milkfactory.dto.QualityRequestDTO;
import java.util.List;

public interface QualityRequestFacade {
    List<QualityRequestDTO> getRequests(Long requestId, String date);
}
