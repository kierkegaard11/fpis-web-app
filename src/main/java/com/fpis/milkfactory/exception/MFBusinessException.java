package com.fpis.milkfactory.exception;

public class MFBusinessException extends MFException {
    private int status;
    private String code;
    private String message;
    private String severity;

    public MFBusinessException(int status, String code, String message, String severity) {
        super(message);
        this.status = status;
        this.code = code;
        this.message = message;
        this.severity = severity;
    }

    public MFBusinessException(int status, String code, String message, String severity, Throwable throwable) {
        super(throwable);
        this.status = status;
        this.code = code;
        this.message = message;
        this.severity = severity;
    }

    public int getStatus() {
        return status;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public String getSeverity() {
        return severity;
    }
}
