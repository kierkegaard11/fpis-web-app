package com.fpis.milkfactory.exception;

public class MFException extends RuntimeException{

    public MFException() {
    }

    public MFException(String message) {
        super(message);
    }

    public MFException(Throwable cause) {
        super(cause);
    }
}
