package com.fpis.milkfactory.exception;

import com.fpis.milkfactory.util.MFErrors;

public class InvalidEntityIdException extends MFBusinessException{

    public InvalidEntityIdException(String message) {
        super(MFErrors.INVALID_ENTITY_ID.getCode(), "InvalidEntityIdException", message, MFErrors.INVALID_ENTITY_ID.getLevel());
    }

    public InvalidEntityIdException(String code,String message) {
        super(MFErrors.INVALID_ENTITY_ID.getCode(), code, message, MFErrors.INVALID_ENTITY_ID.getLevel());
    }
}
