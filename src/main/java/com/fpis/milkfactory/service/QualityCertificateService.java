package com.fpis.milkfactory.service;

import com.fpis.milkfactory.exception.InvalidEntityIdException;
import com.fpis.milkfactory.persistence.dao.QualityCertificateDAO;
import com.fpis.milkfactory.persistence.model.QualityCertificate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class QualityCertificateService {

    private static final String INVALID_CERTIFICATE_ID = "Invalid certificate id";
    private static final String CERTIFICATE_NOT_FOUND_BY_ID = "A quality certificate with id %s does not exist!";

    @Resource
    private QualityCertificateDAO qualityCertificateDAO;

    public QualityCertificate saveQualityCertificate(QualityCertificate qualityCertificate) {
        return qualityCertificateDAO.save(qualityCertificate);
    }

    public List<QualityCertificate> getCertificates(Long certificateId, String date) {
        if (date != null) {
            String stringArray[] = date.split("-");
            Integer year = Integer.valueOf(stringArray[0]);
            Integer month = Integer.valueOf(stringArray[1]);
            Integer day = Integer.valueOf(stringArray[2]);
            return qualityCertificateDAO.findAllByIdAndDate(certificateId, day, month, year);
        }
        return qualityCertificateDAO.findAllByIdAndDate(certificateId, null, null, null);
    }

    public QualityCertificate updateQualityCertificate(Long id, QualityCertificate qualityCertificate) {
        if (qualityCertificateDAO.findByIdAndShit(id).isPresent()) {
            return qualityCertificateDAO.save(qualityCertificate);
        } else {
            throw new InvalidEntityIdException(
                    INVALID_CERTIFICATE_ID,
                    String.format(CERTIFICATE_NOT_FOUND_BY_ID, id)
            );
        }
    }

    public void deleteQualityCertificate(QualityCertificate qualityCertificate) {
        if (!qualityCertificateDAO.findById(qualityCertificate.getId()).isPresent()) {
            throw new InvalidEntityIdException(
                    INVALID_CERTIFICATE_ID,
                    String.format(CERTIFICATE_NOT_FOUND_BY_ID, qualityCertificate.getId())
            );
        }
        qualityCertificateDAO.delete(qualityCertificate);
    }
}
