package com.fpis.milkfactory.service;

import com.fpis.milkfactory.persistence.dao.CustomerReceiptPaymentMethodDAO;
import com.fpis.milkfactory.persistence.model.CustomerReceiptPaymentMethodAggr;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CustomerReceiptPaymentMethodAggrService {
    @Resource
    private CustomerReceiptPaymentMethodDAO customerReceiptPaymentMethodDAO;

    public void saveCustomerReceiptPaymentmethodAggr(CustomerReceiptPaymentMethodAggr customerReceiptPaymentMethodAggr) {
        customerReceiptPaymentMethodDAO.save(customerReceiptPaymentMethodAggr);
    }

    public List<CustomerReceiptPaymentMethodAggr> getPaymentMethodsByReceiptId(Long customerReceiptId) {
        return customerReceiptPaymentMethodDAO.findByCustomerReceiptId(customerReceiptId);
    }

    public void deletecustomerReceiptPaymentMethodAggr(CustomerReceiptPaymentMethodAggr customerReceiptPaymentMethodAggr) {
        customerReceiptPaymentMethodDAO.delete(customerReceiptPaymentMethodAggr);
    }
}
