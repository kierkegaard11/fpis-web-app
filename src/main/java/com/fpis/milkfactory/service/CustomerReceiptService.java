package com.fpis.milkfactory.service;

import com.fpis.milkfactory.dto.CustomerReceiptDTO;
import com.fpis.milkfactory.exception.InvalidEntityIdException;
import com.fpis.milkfactory.persistence.dao.CustomerReceiptDAO;
import com.fpis.milkfactory.persistence.model.CustomerReceipt;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CustomerReceiptService {

    private static final String INVALID_CUSTOMER_RECEIPT_ID =
            "Invalid Customer Receipt ID";
    private static final String CUSTOMER_RECEIPT_NOT_FOUND_BY_ID =
            "A customer receipt with id %s does not exist";

    @Resource
    private CustomerReceiptDAO customerReceiptDAO;

    public CustomerReceipt saveCustomerReceipt(CustomerReceipt customerReceipt) {
        return customerReceiptDAO.save(customerReceipt);
    }

    public void deleteCustomerReceipt(CustomerReceipt customerReceipt) {
        if (!customerReceiptDAO.findById(customerReceipt.getId()).isPresent()) {
            throw new InvalidEntityIdException(INVALID_CUSTOMER_RECEIPT_ID,
                    String.format(CUSTOMER_RECEIPT_NOT_FOUND_BY_ID,
                            customerReceipt.getId()));
        }
        customerReceiptDAO.delete(customerReceipt);
    }

    public CustomerReceipt getCustomerReceiptById(long customerReceiptId) {
        return customerReceiptDAO.findById(customerReceiptId).orElseThrow(() ->
                new InvalidEntityIdException(INVALID_CUSTOMER_RECEIPT_ID,
                        String.format(CUSTOMER_RECEIPT_NOT_FOUND_BY_ID,
                                customerReceiptId)));
    }

    public List<CustomerReceipt> getCustomerReceiptsByIdAndDate(Long customerReceiptId, String date) {
        if (date != null) {
            String stringArray[] = date.split("-");
            Integer year = Integer.valueOf(stringArray[0]);
            Integer month = Integer.valueOf(stringArray[1]);
            Integer day = Integer.valueOf(stringArray[2]);
            return customerReceiptDAO.findAllByIdAndDate(customerReceiptId, day, month, year);
        }
        return customerReceiptDAO.findAllByIdAndDate(customerReceiptId, null, null, null);
    }
}
