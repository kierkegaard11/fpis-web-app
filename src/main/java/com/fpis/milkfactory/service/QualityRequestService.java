package com.fpis.milkfactory.service;

import com.fpis.milkfactory.exception.InvalidEntityIdException;
import com.fpis.milkfactory.persistence.dao.QualityRequestDAO;
import com.fpis.milkfactory.persistence.model.QualityRequest;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class QualityRequestService {
    private static final String INVALID_REQUEST_ID = "Invalid quality certificate id";
    private static final String REQUEST_NOT_FOUND_BY_ID = "A quality request with id %s does not exist";

    @Resource
    private QualityRequestDAO qualityRequestDAO;

    public List<QualityRequest> getRequests(Long requestId, String date) {
        if (date != null) {
            String stringArray[] = date.split("-");
            Integer year = Integer.valueOf(stringArray[0]);
            Integer month = Integer.valueOf(stringArray[1]);
            Integer day = Integer.valueOf(stringArray[2]);
            return qualityRequestDAO.findAllByIdAndDate(requestId, day, month, year);
        }
        return qualityRequestDAO.findAllByIdAndDate(requestId, null, null, null);
    }

    public QualityRequest getQualityRequestById(Long id) {
        return qualityRequestDAO.findById(id)
                .orElseThrow(() -> new InvalidEntityIdException(
                        INVALID_REQUEST_ID,
                        String.format(REQUEST_NOT_FOUND_BY_ID, id)
                ));
    }
}
