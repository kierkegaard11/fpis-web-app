package com.fpis.milkfactory.service;

import com.fpis.milkfactory.exception.InvalidEntityIdException;
import com.fpis.milkfactory.persistence.dao.CustomerReceiptItemDAO;
import com.fpis.milkfactory.persistence.model.CustomerReceiptItem;
import org.apache.logging.log4j.message.StringFormattedMessage;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CustomerReceiptItemService {
    private static final String CUSTOMER_RECEIPT_ITEM_NOT_FOUND_BY_ID =
            "A customer receipt item with id = %s does not exist.";
    private static final String INVALID_CUSTOMER_RECEIPT_ITEM_ID =
            "Invalid Customer Receipt Item ID";

    @Resource
    private CustomerReceiptItemDAO customerReceiptItemDAO;

    public CustomerReceiptItem saveCustomerReceiptItem(CustomerReceiptItem customerReceiptItem) {
        return customerReceiptItemDAO.save(customerReceiptItem);
    }

    public List<CustomerReceiptItem> getItemsByReceiptId(long customerReceiptId) {
        return customerReceiptItemDAO.getByCustomerReceiptId(customerReceiptId);
    }

    public void deleteCustomerReceiptItem(CustomerReceiptItem itemToDelete) {
        if (!customerReceiptItemDAO.findById(itemToDelete.getId()).isPresent()) {
            throw new InvalidEntityIdException(INVALID_CUSTOMER_RECEIPT_ITEM_ID,
                    String.format(CUSTOMER_RECEIPT_ITEM_NOT_FOUND_BY_ID,
                            itemToDelete.getId()));
        } else {
            customerReceiptItemDAO.delete(itemToDelete);
        }
    }

    public void updateCustomerReceipt(CustomerReceiptItem newItem) {
        if (!customerReceiptItemDAO.findById(newItem.getId()).isPresent()) {
            throw new InvalidEntityIdException(INVALID_CUSTOMER_RECEIPT_ITEM_ID,
                    String.format(CUSTOMER_RECEIPT_ITEM_NOT_FOUND_BY_ID,
                            newItem.getId()));
        } else {
            customerReceiptItemDAO.update(newItem.getId(),
                    newItem.getProduct(),
                    newItem.getQuantity(),
                    newItem.getRebate(),
                    newItem.getPriceWithDiscount());
        }
    }
}
