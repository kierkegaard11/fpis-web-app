package com.fpis.milkfactory.service;

import com.fpis.milkfactory.persistence.dao.WorkerDAO;
import com.fpis.milkfactory.persistence.model.Worker;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class WorkerService {

    @Resource
    private WorkerDAO workerDAO;

    public List<Worker> getWorkers(){
        return workerDAO.findAll();
    }
}
