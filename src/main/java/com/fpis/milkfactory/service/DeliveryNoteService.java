package com.fpis.milkfactory.service;

import com.fpis.milkfactory.exception.InvalidEntityIdException;
import com.fpis.milkfactory.persistence.dao.DeliveryNoteDAO;
import com.fpis.milkfactory.persistence.model.DeliveryNote;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class DeliveryNoteService {

    private static final String INVALID_DELIVERY_NOTE_ID = "Invalid delivery note id!";
    private static final String DELIVERY_NOTE_NOT_FOUND_BY_ID = "Delivery note with id=%s does not exist!";
    @Resource
    private DeliveryNoteDAO deliveryNoteDAO;

    public List<DeliveryNote> getDeliveryNotes(Long noteId, String date) {
        if (date != null) {
            String stringArray[] = date.split("-");
            Integer year = Integer.valueOf(stringArray[0]);
            Integer month = Integer.valueOf(stringArray[1]);
            Integer day = Integer.valueOf(stringArray[2]);
            return deliveryNoteDAO.findAllByIdAndDate(noteId, day, month, year);
        }
        return deliveryNoteDAO.findAllByIdAndDate(noteId, null, null, null);
    }

    public DeliveryNote getDeliveryNoteById(Long id) {
        return deliveryNoteDAO.findById(id).orElseThrow(() ->
                new InvalidEntityIdException(INVALID_DELIVERY_NOTE_ID,
                        String.format(DELIVERY_NOTE_NOT_FOUND_BY_ID,
                                id)));
    }
}
