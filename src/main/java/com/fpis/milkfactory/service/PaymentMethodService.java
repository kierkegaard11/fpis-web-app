package com.fpis.milkfactory.service;

import com.fpis.milkfactory.exception.InvalidEntityIdException;
import com.fpis.milkfactory.persistence.dao.PaymentMethodDAO;
import com.fpis.milkfactory.persistence.model.PaymentMethod;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class PaymentMethodService {

    private static final String PAYMENT_METHOD_NOT_FOUND_BY_ID =
            "A payment method with id = %s does not exist.";
    private static final String INVALID_PAYMENT_METHOD =
            "Invalid Payment Method ID";

    @Resource
    private PaymentMethodDAO paymentMethodDAO;

    public List<PaymentMethod> getPaymentMethods(){
        return paymentMethodDAO.findAll();
    }

    public PaymentMethod getPaymentMethodById(Long id) {
        return paymentMethodDAO.findById(id)
                .orElseThrow(() -> new InvalidEntityIdException(
                        INVALID_PAYMENT_METHOD,
           String.format(PAYMENT_METHOD_NOT_FOUND_BY_ID, id)
            ));
    }

}
