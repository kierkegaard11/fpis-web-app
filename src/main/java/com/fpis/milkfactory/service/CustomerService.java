package com.fpis.milkfactory.service;

import com.fpis.milkfactory.exception.InvalidEntityIdException;
import com.fpis.milkfactory.persistence.model.Customer;
import com.fpis.milkfactory.persistence.dao.CustomerDAO;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class CustomerService {

    private static final String CUSTOMER_NOT_FOUND_BY_ID =
            "A customer with id = %s does not exist.";
    private static final String INVALID_CUSTOMER_ID =
            "Invalid Customer ID";

    @Resource
    private CustomerDAO customerDAO;

    public Customer getCustomerById(Long id) {
    return customerDAO.findById(id)
            .orElseThrow(() -> new InvalidEntityIdException(
                    INVALID_CUSTOMER_ID,
                    String.format(CUSTOMER_NOT_FOUND_BY_ID, id)
            ));
    }
}
