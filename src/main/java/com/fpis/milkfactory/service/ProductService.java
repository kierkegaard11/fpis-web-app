package com.fpis.milkfactory.service;

import com.fpis.milkfactory.persistence.dao.ProductDAO;
import com.fpis.milkfactory.persistence.model.Product;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ProductService {

    @Resource
    private ProductDAO productDAO;

    public List<Product> getProducts(Long productId, String name) {
        return productDAO.findAllByNameAndId(productId, name);
    }
}
