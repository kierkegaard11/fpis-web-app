package com.fpis.milkfactory;

import com.fpis.milkfactory.config.MilkFactoryDataSourceConfiguration;
import com.fpis.milkfactory.config.SwaggerConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.HashMap;
import java.util.Map;


@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class, DataSourceTransactionManagerAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
@PropertySource(value = {
        // @formatter:off
        "classpath:static.properties",
        "classpath:dynamic.properties",
        "classpath:secure.properties",
        "file:///${APPS_CONFIG}/fpis/static.properties",
        "file:///${APPS_CONFIG}/fpis/dynamic.properties",
        "file:///${APPS_CONFIG}/fpis/secure.properties"
        // @formatter:on
}, ignoreResourceNotFound = true)
@Import({MilkFactoryDataSourceConfiguration.class, SwaggerConfiguration.class})

public class MilkFactoryApp {
    private static final Logger LOGGER = LoggerFactory.getLogger(MilkFactoryApp.class);

    @Autowired
    private Environment env;

    public static void main(String[] args) {
        SpringApplication.run(MilkFactoryApp.class, args);
        LOGGER.info("Milk Factory application started");
    }

    @Bean
    public EntityManagerFactoryBuilder entityManagerFactoryBuilder() {
        Map<String, Object> properties = new HashMap<>();
        properties.put("hibernate.dialect", env.getProperty("spring.jpa.properties.hibernate.dialect"));
        properties.put("hibernate.hbm2ddl.auto", env.getProperty("spring.jpa.hibernate.ddl-auto"));

        return new EntityManagerFactoryBuilder(new HibernateJpaVendorAdapter(), properties, null);
    }
}





